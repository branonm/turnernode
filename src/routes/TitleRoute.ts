import {Router, Request, Response, NextFunction} from 'express';
import {MongoClient} from "mongodb";

// TODO: make configurable
const mongoURL = "mongodb://readonly:turner@ds043348.mongolab.com:43348/dev-challenge";

export class TitleRouter {
    router: Router;

    constructor() {
        this.router = Router();
        this.router.get('/', this.getTitles);
        this.router.get('/:id', this.getTitleById);
    }

    public getTitles(req: Request, res: Response, next: NextFunction) {
       let mongoClient = new MongoClient();

       mongoClient.connect(mongoURL, (err, db) => {

           if (err) {
                console.log('Error: ', err);
                res.status(503).send(err);
            }
            else {

                console.log("Connected to Mongo.")
                console.log("Retrieving titles.");
                let collection = db.collection('Titles');

               collection.find().toArray().then(function(docs) {

                   // TODO: Handle zero docs returned
                   let titles : Array<[string, string]> = new Array(docs.length);
                   let offset : number = 0;
                   for(let doc of docs)
                   {
                        titles[offset++] = [doc.TitleName, doc._id]
                   }
                    res.status(200).send(titles);
                }).catch( /*Do something sane*/);

               }
        });
    }

    public getTitleById(req: Request, res: Response, next: NextFunction) {
        let id:string = req.params.id;

        let mongoClient = new MongoClient();
        mongoClient.connect(mongoURL, (err, db) => {
            if (err) {
                console.log('Error: ', err);
                res.status(503).send(err);
            }
            else {

                console.log("Connected to Mongo.")
                console.log("Retrieving titles.");
                let collection = db.collection('Titles');
                collection.findOne({"_id":id}).then((doc) =>
                {
                    res.status(200).send(doc)
                }).catch(/**/);
             }
        });
    }
}


const TitleRoutes = new TitleRouter();
export default TitleRoutes.router;